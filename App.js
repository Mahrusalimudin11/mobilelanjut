import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';
import React, {Component} from 'react';

class PartSatu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      warnaText: 'black',
      inputPertama: null,
      inputKedua: null,
      fullText: null,
    };
  }

  getInputSatu = text => {
    this.setState({inputPertama: text});
    // console.log(this.state.inputPertama);
  };

  getInputDua = text => {
    this.setState({inputKedua: text});
    // console.log(this.state.inputPertama);
  };

  ubahWarna = () => {
    // ambil warna
    const warna = this.state.warnaText;
    // cek warna
    if (warna === 'black') {
      // jika warna black
      this.setState({warnaText: 'red'}); // ganti menjadi red
    } else if (warna === 'red') {
      this.setState({warnaText: 'yellow'});
    } else if (warna === 'yellow') {
      this.setState({warnaText: 'green'});
    } else {
      this.setState({warnaText: 'black'});
    }
    // ternary condition
    // warna === 'black'
    //   ? this.setState({warnaText: 'red'})
    //   : this.setState({warnaText: 'black'});
  };

  updateFullText = () => {
    let text1 = this.state.inputPertama;
    let text2 = this.state.inputKedua;
    if (text1 !== null && text2 !== null && text1 !== ' ' && text2 !== ' ') {
      this.setState({fullText: text1 + ' ' + text2});
    } else {
      Alert.alert('Info', 'Input tidak boleh kosong');
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={{color: this.state.warnaText}}>Shollutech Official</Text>
        <TextInput
          onChangeText={text => this.getInputSatu(text)}
          style={styles.input}
        />
        <TextInput
          onChangeText={text => this.getInputDua(text)}
          style={styles.input}
        />
        <Text>full text : {this.state.fullText}</Text>
        <TouchableOpacity
          onPress={() => this.updateFullText()}
          style={styles.btn}>
          <Text>Tekan</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default PartSatu;

const styles = StyleSheet.create({
  container: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: 'orange',
    padding: 5,
  },
  input: {
    backgroundColor: 'yellow',
    margin: 10,
    width: '70%',
    paddingHorizontal: 10,
    height: 45,
    borderColor: 'black',
    borderWidth: 4,
    fontSize: 18,
  },
});
